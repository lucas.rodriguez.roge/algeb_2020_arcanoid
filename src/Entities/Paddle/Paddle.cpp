#include "Paddle.h"
#include "../../Game/Game.h"
#include "..\..\Scenes\Gameplay\Gameplay.h"

namespace game
{
	namespace paddle
	{
		const Vector2 paddleSize = { 70, 15 }; //paddleSize = { width, height }

		Paddle paddle;

		void GeneratePaddle()
		{
			//Paddle Size
			paddle.body.width = paddleSize.x; //assign paddle width
			paddle.body.height = paddleSize.y; //assign paddle height
			//Paddle Position
			paddle.body.x = game::screenWidth / 2 - paddleSize.x / 2;
			paddle.body.y = game::screenHeight - (paddleSize.y / 2) * 5;
			//Paddle Color
			paddle.color = RED;
			//Paddle Controls
			paddle.controls.left = KEY_LEFT;
			paddle.controls.right = KEY_RIGHT;
			paddle.maxSpeed = 2;
		}

		bool PaddleIsMovingWithinMap(bool paddleIsMovingLeft)
		{
			//check if the paddle's position after moving will be inside of the map
			if (paddleIsMovingLeft)
			{
				return paddle::paddle.body.x - paddle::paddle.maxSpeed > 0;
			}
			else
			{
				return paddle::paddle.body.x + paddle::paddle.body.width + paddle::paddle.maxSpeed < game::screenWidth;
			}
		}

		void MovePaddle()
		{
			const int timeMultiplier = 100;
			//move the player in the direction according to the key he pressed and modify
			//internal speed value (speed is used in collisions between players and ball)
			if (IsKeyDown(paddle::paddle.controls.left) && PaddleIsMovingWithinMap(true))
			{
				paddle::paddle.speed = -paddle::paddle.maxSpeed * GetFrameTime() * timeMultiplier;
				paddle::paddle.body.x -= paddle::paddle.maxSpeed * GetFrameTime() * timeMultiplier;

			}
			else if (IsKeyDown(paddle::paddle.controls.right) && PaddleIsMovingWithinMap(false))
			{
				paddle::paddle.speed = paddle::paddle.maxSpeed * GetFrameTime() * timeMultiplier;
				paddle::paddle.body.x += paddle::paddle.maxSpeed * GetFrameTime() * timeMultiplier;
			}
			else
			{
				paddle::paddle.speed = 0;
			}
		}

		void ShowPaddle()
		{
			DrawRectangleRec(paddle::paddle.body, paddle::paddle.color);
		}
	}
}