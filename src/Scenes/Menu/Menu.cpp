#include "Menu.h"
#include "..\..\Game\Game.h"

namespace game
{
	namespace menu
	{
		int option;
		const Color optionColorSelect = YELLOW;
		const Color optionColorNoSelect = LIGHTGRAY;

		void Init()
		{
			option = 1;
		}

		void Update()
		{
			MoveOption();
			if (AcceptOption())
			{
				switch ((MAIN_MENU)option)
				{
				case MAIN_MENU::PLAY:
					gameplay::init();
					game::gameStatus = GAME_STATUS::INGAME;
					break;
				case MAIN_MENU::TEST:
					test::init();
					game::gameStatus = GAME_STATUS::TEST;
					break;
				case MAIN_MENU::CREDITS:
					//game::gameStatus = GAME_STATUS::CREDITS;
					break;
				case MAIN_MENU::EXIT:
					game::gameStatus = GAME_STATUS::EXIT;
					break;
				default:
					break;
				}
			}
		}

		void Draw()
		{
			const char* text = "ARKANOID";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = 50;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			posY = game::screenHeight / 2;
			font = 20;
			int spacing = font + 10;
			DrawText("PLAY", (posX - (MeasureText("PLAY", font) / 2)), (posY - (font / 2)), font, GetColorOptionMenu((int)MAIN_MENU::PLAY));
			DrawText("TEST", (posX - (MeasureText("TEST", font) / 2)), ((posY + spacing) - (font / 2)), font, GetColorOptionMenu((int)MAIN_MENU::TEST));
			DrawText("CREDITS", (posX - (MeasureText("CREDITS", font) / 2)), ((posY + spacing * 2) - (font / 2)), font, GetColorOptionMenu((int)MAIN_MENU::CREDITS));
			DrawText("EXIT", (posX - (MeasureText("EXIT", font) / 2)), ((posY + (spacing * 3)) - (font / 2)), font, GetColorOptionMenu((int)MAIN_MENU::EXIT));
		}

		void DeInit()
		{
		}

		bool CheckOptionMainMenu(int auxOption)
		{
			switch ((MAIN_MENU)auxOption)
			{
			case MAIN_MENU::PLAY:
			case MAIN_MENU::TEST:
			case MAIN_MENU::CREDITS:
			case MAIN_MENU::EXIT:
				return true;
			default:
				return false;
			}
		}

		int GetOptionInput()
		{
			if (IsKeyPressed(KEY_UP))
			{
				return (option - 1);
			}
			if (IsKeyPressed(KEY_DOWN))
			{
				return (option + 1);
			}

			return option;
		}

		void MoveOption()
		{
			if (IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_DOWN))
			{
				int auxOption = GetOptionInput();

				if (CheckOptionMainMenu(auxOption))
				{
					option = auxOption;
				}
			}
		}

		bool AcceptOption()
		{
			if (IsKeyPressed(KEY_ENTER))
			{
				return true;
			}

			return false;
		}

		Color GetColorOptionMenu(int auxOption)
		{
			if (option == auxOption)
			{
				return optionColorSelect;
			}

			return optionColorNoSelect;
		}
	}
}